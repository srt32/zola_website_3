**Personal Static Website**

In this repo I create a personal webiste, set up a CI/CD with GitLabs, and deploy it on Netlify. 

1. I chose Netlify because it appeared to be the easiest hosting platform when compared to Vercel, Amazon Amplify, and Amazon S3. 

2. To create the static website I retrieved a Zola template from https://www.getzola.org/

3. From there, I followed the instructions on my template's website & began making adjustments to make the website personal to me. 

4. After I created the content for the website, I set up the CI/CD process on GitLabs by adding a config.toml file to my repo. 

5. After troubleshooting my website's CI/CD pipeline, I set up deployment on Netlify. I created an account, connected it to my GitLab account, and added a netlify.toml file to my repo. 

My website can be found at: https://65ee70f6c5414fe973d2ee5d--subtle-monstera-0d8bfa.netlify.app/

