+++
title = "Suzanna's Portfolio"
images = ["82320d46-37fb-48fd-9cdb-0c9100d968c4.png"]
template = "index.html"
in_search_index = true
+++


<style>
  .large-text {
    font-size: 28px;
    font-weight: bold; /* This will make the text bold */
  }
</style>

<p style="line-height: 1.6;">
  <img src="82320d46-37fb-48fd-9cdb-0c9100d968c4.png" alt="Description" style="float: left; margin-right: 6px;" width="200" />

<p class="large-text">
A bit about me:

</p>

<p>
   I'm a graduate student at Duke University, working towards a Master's degree in Interdisciplinary Data Science and expect to graduate in May 2024.

   Before beginning graduate school, I recieved a Bachelor's Degree in Mathematics from Winthrop University in Rock Hill. During my time there, I was recognized on the Dean’s list, contributed to the Dean’s Committee, and was honored with the Ellen Rasor Wylie Mathematics Scholarship.

   When it comes to technical skills, I’m proficient in programming languages including Python, R, SQL, C++, and Rust. I also have hands-on experience with database and big data technologies, as well as visualization tools like PowerBI, Tableau, Databricks, Bitbucket, MongoDB, Delta Lake, Hive, Google Cloud Platform, and BigQuery.

   Currently, my capstone project is partnered with the United States Army, where we are focusing on utilizing recruitment data to enhance the hiring efficiency within the Special Operations organization. Concurrently, I lead a research subteam at my university and the VA, overseeing a comprehensive project that aims to curate, preprocess, and web scrape a vast array of research articles on noninvasive neuromodulation. We’re developing an automated summarization system using GPT to provide updated summaries and relevant metadata for a continuously refreshed wiki.

  </p>


