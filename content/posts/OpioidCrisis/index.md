+++
title = "Opioid Crisis Analysis"
date = 2022-12-01
[taxonomies]
tags= ["Zola", "Theme", "gallery"]
[extra]
images= []
+++




[Project repo](https://github.com/srt3264/opioid-crisis-project/tree/main)

For this project, we are trying to address the problem of rising opioid usage and its connection with death by drugs in the United States. We have two specific questions: What effect did the opioid policies in Texas, Washington, and Florida have on the number of opioid deaths and overall drug deaths in those states? In other words, how do opioid deaths and overall drug deaths differ in Texas, Washington, and Florida from our chosen control states?

Using pre-post difference-in-difference analysis, we created two final reports, one for data scientists and one for policy makers.

## Members: 
Beibei Du, Wafiakmal Miftah, Suzanna Thompson, Alisa Tian (sort alphabetically by last name)

## Motivation
The opioid overdose usage is a devastating and rapidly-escalating public/global health concern. Opioids have become alarmingly prevalent and have contributed to overdose deaths. This crisis is particularly severe in the United States, and its impact extends beyond individual users to affect entire communities or even around the globe. It is vital that individuals, communities, and policymakers be aware of this issue and take action to address it. Thus implementing policies to decrease access to these drugs could be one of the solutions to combat the opioid overdose crisis. However, we need to evaluate if the policies are effective for some of the states that implement the policy changes. Thus we chose 3 states and their controlling states to see if there is a negative trend in the death rate and drug amont after the policy change using the two methods: diff-in-diff model and pre-post model.


## Link to the Presentaion
[Video](https://www.youtube.com/watch?v=YDcqPdnvGS0)


