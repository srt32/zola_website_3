+++
title = "Wikistim Summarization"
date = 2023-08-01
[taxonomies]
tags= ["Zola", "Theme", "gallery"]
[extra]
images= []
+++



[Project repo](https://github.com/srt3264/Wikistim-Summarization)

## Project Overview
The 'Wikistim Summarization' project aims to develop a robust data retrieval pipeline dedicated to enhancing the accessibility and efficiency of scientific literature on noninvasive and invasive neuromodulation. The project utilizes advanced algorithms to download research papers, scrape the textual content, and organize it into a structured format.

## Goals
- **Increased Access**: Broaden the availability of scientific literature and data pertaining to neuromodulation.
- **Review Efficiency**: Streamline the literature review process, enabling faster and more comprehensive access to relevant studies.
- **Report Quality**: Elevate the quality of reports and the underlying study design through meticulous data organization and summarization.

## Pipeline Workflow
1. **Data Retrieval**: Download research papers related to neuromodulation.
2. **Text Scraping**: Extract textual content from the papers and write to a structured table.
3. **Metadata Extraction**: Use GPT-4 to identify and extract metadata such as number of participants, study design, and statistical significance of results.
4. **Database Population**: Populate a database with the extracted information for further analysis and review.
5. **Web Integration**: Upload the organized database to [Wikistim](https://www.wikistim.org/) for public access.

## Technologies Used
- GPT-4 for metadata extraction and data summarization
- Custom scraping and data retrieval scripts
- Database management systems



