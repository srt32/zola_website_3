+++
title = "Classifying Brain Tumors using MRI Data"
date = 2023-05-01
[taxonomies]
tags= ["classification", "image date", "CNN"]
[extra]
images= []
+++




[Project repo](https://github.com/srt3264/Brain-Tumor-MRI-Classification)

## Overview
This project aims to build a classification model on brain tumor data obtained from the Brain Tumor MRI Dataset on Kaggle. The data consists of magnetic resonance imaging (MRI) scans of the brain, with each scan labeled as either having a tumor or being a healthy scan.

The project has two main phases:

1. **Building the classification model**: In this phase, we use the brain tumor MRI dataset to train a baseline classification model that can distinguish between tumor and healthy brain scans with high accuracy.
2. **Adding synthetic noise and other conditions to the data**: In this phase, we introduce synthetic noise, motion blur, and synthetically lower the resolution to the brain MRI scans to simulate real-world scenarios where scans may be noisy or distorted. We then test the previously built classification model on the noisy data to see how well it still works.
3. **Retraining the classifier using the noisy data**: In this phase, we retrain the classification model using the noisy data to see if it performs better on the noisy data.


[Here is a link to the final paper](https://github.com/srt3264/Brain-Tumor-MRI-Classification/blob/main/Final_Paper.pdf)


[Here is a link to the final presentation](https://github.com/srt3264/Brain-Tumor-MRI-Classification/blob/main/ML%20imaging%20powerpoint.pdf)



